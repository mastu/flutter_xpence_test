package com.example.flutter_onfido_test

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.onfido.android.sdk.capture.OnfidoConfig
import com.onfido.android.sdk.capture.OnfidoFactory
import com.onfido.api.client.OnfidoAPI
import com.onfido.api.client.OnfidoAPIFactory
import com.onfido.api.client.data.Applicant

import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.onfido.android.sdk.capture.ExitCode
import com.onfido.android.sdk.capture.Onfido
import com.onfido.android.sdk.capture.errors.OnfidoException
import com.onfido.android.sdk.capture.ui.camera.face.FaceCaptureStep
import com.onfido.android.sdk.capture.ui.camera.face.FaceCaptureVariant
import com.onfido.android.sdk.capture.ui.camera.face.FaceCaptureVariantVideo
import com.onfido.android.sdk.capture.ui.options.FlowStep
import com.onfido.android.sdk.capture.upload.Captures
import org.json.JSONObject

class MainActivity : FlutterActivity() {
    private val CHANNEL = "test.panov.it/onfido"
    private val startOnfidoWorkflow = "startOnfidoWorkflow"
    private val ONFIDO_REQUEST_CODE = 1111
    private var mobileToken = ""
    private var apiToken = ""

    private lateinit var onfidoHandler: (requestCode:Int, resultCode: Int, data: Intent?)-> Unit


    companion object {
        private var instance: Activity? = null
        var mainActivity: Activity? = null
           get() = instance
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)
        instance = this
        MethodChannel(flutterView, CHANNEL).setMethodCallHandler { call, result ->
            if (call.method == startOnfidoWorkflow) {
                mobileToken = call.argument("mobileToken")!!
                apiToken = call.argument("apiToken")!!
                Log.i("Onfido", "Mobile Token $mobileToken")
                Log.i("Onfido", "Api Token $apiToken")

                createApplicant(object : JSONObjectRequestListener {
                    override fun onError(anError: ANError?) {
                       Log.e("Onfido",anError?.localizedMessage + anError?.errorBody + anError?.errorCode)
                    }

                    override fun onResponse(response: JSONObject?) {
                        try {
                            val applicantId = response?.getString("id")
                            Log.d("Onfido",applicantId)
                            val onfido = OnfidoFactory.create(MainActivity.mainActivity!!).client
                            val config = OnfidoConfig.builder()
                                    .withToken(mobileToken)
                                    .withApplicant(applicantId!!)
                                    .withCustomFlow(arrayOf(FaceCaptureStep(FaceCaptureVariant.VIDEO)))
                                    .build()

                            onfidoHandler = {requestCode, resultCode, data ->
                                onfido.handleActivityResult(resultCode,data, object : Onfido.OnfidoResultListener{
                                    override fun onError(exception: OnfidoException, applicant: Applicant?) {
                                        Log.e("Onfido", exception.localizedMessage)
                                        result.error("Onfido Error",exception.localizedMessage,null)
                                    }

                                    override fun userCompleted(applicant: Applicant, captures: Captures) {
                                        Log.e("Onfido", "Capture Completed with ApplicantId ${applicant.id}")
                                        result.success(applicant.id)
                                    }

                                    override fun userExited(exitCode: ExitCode, applicant: Applicant) {
                                        Log.i("Onfido", "User Exited")
                                        result.success("")
                                    }
                                })
                            }

                            onfido.startActivityForResult(MainActivity.mainActivity!!, ONFIDO_REQUEST_CODE, config)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                })


            } else {
                result.notImplemented()
            }
        }
    }

    private fun createApplicant(listener: JSONObjectRequestListener) {
        var applicant = JSONObject();
        applicant.put("first_name", "DummyX")
        applicant.put("last_name", "DummyF")
        AndroidNetworking.post("https://api.onfido.com/v2/applicants")
                .addJSONObjectBody(applicant)
                .addHeaders("Accept", "application/json")
                .addHeaders("Authorization", "Token token=$apiToken")
                .build()
                .getAsJSONObject(listener)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == ONFIDO_REQUEST_CODE){
            onfidoHandler.invoke(requestCode,resultCode,data)
        }
    }
}
