import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

const String API_TOKEN = "test_PcjVGJIGQCLbq7wq3o8LF8t-XNB3Ybal";
const String MOBILE_TOKEN = "test_DY64EaN3vvSp_S1s8fOnMsohqQSD8KXX";

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  
  String _applicantId = "";
  File _image;
  Image _imageView = Image.asset("assets/images/no-image-available.png");

  static const platform = const MethodChannel('test.panov.it/onfido');

  Future<void> _getOnfidoApplicantId(
      String apiToken, String mobileToken) async {
    String result = '';
    try {
      result = await platform.invokeMethod('startOnfidoWorkflow',
          {"mobileToken": mobileToken, "apiToken": apiToken});
    } on PlatformException catch (e) {
      print(e.message);
    }
    setState(() {
      _applicantId = result;
      print("Applicant Id from native request $_applicantId");
    });
  }

  Future _getImage(ImageSource imageSource) async {
    var image = await ImagePicker.pickImage(source: imageSource);
    setState(() {
      _image = image;
      if(image != null)
      _imageView = Image.file(_image, width: 300, height: 300);
      else{
        Image.asset("assets/images/no-image-available.png");
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return 
        Container(
          decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Color.fromRGBO(98,0,238, 1), Color.fromRGBO(3, 218, 197, 1)])),
          child: Stack(
            children: <Widget>[
              Scaffold(
                backgroundColor: Colors.transparent,
                // appBar: AppBar(
                //   backgroundColor: Colors.transparent,
                //   title: Text(widget.title),
                //),
                body: Center(
                  // Center is a layout widget. It takes a single child and positions it
                  // in the middle of the parent.
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text(
                        'Applicant Id:',
                        style: Theme.of(context)
                            .textTheme
                            .headline
                            .copyWith(color: Colors.blueAccent),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Text(
                        '$_applicantId',
                        style: Theme.of(context)
                            .textTheme
                            .body2
                            .copyWith(color: Colors.amber),
                      ),
                      _imageView
                    ],
                  ),
                ),
                bottomNavigationBar: Padding(
              padding: EdgeInsets.symmetric(vertical: 32, horizontal: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  RaisedButton(
                      child: Text("Start Onfido",style: TextStyle(color: Colors.white),),
                      color: Colors.blue,
                      onPressed: () {
                        _getOnfidoApplicantId(API_TOKEN, MOBILE_TOKEN);
                      }),
                  RaisedButton(
                    child: Text("Choose Image",style: TextStyle(color: Colors.white),),
                    color: Colors.amber,
                    onPressed: () {
                      _getImage((ImageSource.gallery));
                    },
                  ),
                  RaisedButton(
                    child: Text("Capture Image",style: TextStyle(color: Colors.white),),
                    color: Colors.greenAccent,
                    onPressed: () {
                      _getImage(ImageSource.camera);
                    },
                  ),
                ],
              )),
                // floatingActionButton: FloatingActionButton(
                //   onPressed: () { _getOnfidoApplicantId(API_TOKEN,MOBILE_TOKEN);}, //_incrementCounter,
                //   tooltip: 'Increment',
                //   child: Icon(Icons.add),
                // ), // This trailing comma makes auto-formatting nicer for build methods.
              ),
               Center(child: Image.asset("assets/images/backgroundSR.png",width: 1500, fit: BoxFit.fitWidth,))
            ],
          ),
        );
    
  }
}
